#!/usr/bin/env python


from pathlib import Path
from flask import Flask, render_template, request
from flask_frozen import Freezer

test_local = False

app = Flask(__name__, static_url_path='/static', static_folder='static')

if not test_local:
    app.config['FREEZER_BASE_URL'] = 'https://mwokeefe.gitlab.io/my-site/'
    app.config['FREEZER_DESTINATION'] = 'public'

freezer = Freezer(app)


@app.cli.command()
def freeze():
    freezer.freeze()


@app.cli.command()
def serve():
    freezer.run()


@freezer.register_generator
def page_generator():
    """
    Frozen-Flask doesn't know what to generate when a route contains a
    variable. This function resolves this, refer to Frozen-Flask's
    documentation for more information.
    """
    for template_path in app.jinja_env.list_templates():
        try:
            page = Path(template_path).relative_to("content").stem
            print(page)
            if "data" in page or "contact" in page: 
                continue

            yield 'pages', {'page': page}
        except ValueError:
            pass


@app.route('/')
def index():
    return render_template("layout.html", pages=page_generator())

@app.route('/<page>/')
def pages(page):
    return render_template(str(Path('content') / (page + '.html')))

@app.route('/contact/', methods=["GET", "POST"])
def contact():
    if request.method == "POST":
        return f"FINALLY METHOD IS POST!!!"

        req = request.form
        print(req)

        return redirect(request.url)

    return render_template(str(Path('content') / ('contact.html')))


if test_local:
    app.run(port=8000)
